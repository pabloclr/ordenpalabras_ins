#!/usr/bin/env pythoh3

'''
Program to order a list of words given as arguments
'''

import sys


def is_lower(first: str, second: str):
    """Return True if first is lower (alphabetically) than second

    Order is checked after lowercasing the letters
    `<` is only used on single characteres.
    """
    primero = first.lower()
    segundo = second.lower()
    lower = False
    for cont in range(len(primero)):
            if primero[cont] < segundo[cont]:
                lower = True
            break
    return lower
def sort_pivot(words: list, pos: int):
    """Sorts word in pivot position, moving it to the left until its place"""

    for alfa in range(pos, len(words)):
        tmp= words[alfa]
        pos = alfa - 1
        if is_lower(words[pos], words[alfa] ) == False:
            words[pos+1]= words[pos]
            pos = pos - 1
            words[alfa - 1] = tmp

    return alfa


def sort(words: list):
    """Return the list of words, ordered alphabetically"""
    alfabeto = []

    for b in range(0,len(words)):
        iden = sort_pivot(words, b)
        alfabeto += [words[iden]]
        words[iden] = words[b]

    return alfabeto


def show(words: list):
    """Show words on screen, using print()"""
    mystring = ''
    for x in words:
        mystring += ' ' + x
    return print(mystring)

def main():
    words: list = sys.argv[1:]
    sort(words)
    show(words)

if __name__ == '__main__':
    main()
